#ifndef __SRENDER_PASS_STATE_H_INCLUDED__
#define __SRENDER_PASS_STATE_H_INCLUDED__

#include "GraphicsConstants.h"
#include <glm/vec4.hpp>
#include <array>

namespace saga
{
namespace video
{

enum E_RENDER_PASS_STATE
{
  CLEAR 
};

struct SColorAttachmentInitialState
{
  E_RENDER_PASS_STATE State;
  glm::vec4 Values;
};

struct SDepthAttachmentInitialState
{
  E_RENDER_PASS_STATE State;
  float Value;
};

struct SStencilAttachmentInitialState
{
  E_RENDER_PASS_STATE State;
  std::uint8_t Value;
};

struct SRenderPassState
{
  SRenderPassState()
    :  Colors({ E_RENDER_PASS_STATE::CLEAR , { 0.5f, 0.5f, 0.5f, 0.5f } }),
       Depth({ E_RENDER_PASS_STATE::CLEAR, 1.f }),
       Stencil({ E_RENDER_PASS_STATE::CLEAR, 0 }) {}
  std::array<SColorAttachmentInitialState, MAX_COLOR_ATTACHMENTS> Colors;
  SDepthAttachmentInitialState Depth;
  SStencilAttachmentInitialState Stencil;
};

} // namespace scene
} // namespace saga

#endif // __SRENDER_PASS_STATE_H_INCLUDED__

