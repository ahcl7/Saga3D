#ifndef __SDEPTH_STENCIL_STATE_H_INCLUDED__
#define __SDEPTH_STENCIL_STATE_H_INCLUDED__

#include "ECompareFunc.h"

namespace saga
{
namespace video
{
  struct SDepthStencilState
  {
    E_COMPARE_FUNC DepthCompareFunc;
    bool EnableDepthWrite;
  };

} // namespace scene
} // namespace saga

#endif // __SDEPTH_STENCIL_STATE_H_INCLUDED__

