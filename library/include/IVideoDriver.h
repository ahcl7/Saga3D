// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __IRR_I_VIDEO_DRIVER_H_INCLUDED__
#define __IRR_I_VIDEO_DRIVER_H_INCLUDED__

// #include "SColor.h"
// #include "ITexture.h"
// #include "plane3d.h"
// #include "IMeshBuffer.h"
// #include "triangle3d.h"
// #include "rect.h"
// #include "EDriverTypes.h"
// #include "EDriverFeatures.h"
// #include "aabbox3d.h"
// #include "EPrimitiveTypes.h"
// #include "EVertexTypes.h"
// #include "SVertexIndex.h"

#include "SRenderPass.h"
#include "SShaderUniform.h"
#include "SPipeline.h"

#include <vector>
#include <string>
#include <glm/vec2.hpp>

namespace saga
{

class SagaDevice;

namespace scene
{
  class IMeshBuffer;
  class ISceneManager;
  // class IMesh;
  // class IMeshManipulator;
  // class ISceneNode;
}

namespace video
{
//   struct S3DVertex;
//   struct S3DVertex2TCoords;
//   struct S3DVertexTangents;
//   struct SLight;
//   class IImageLoader;
//   class IImageWriter;
//   class IRenderTarget;
//   class ITexture;
//   class IImage;
//   enum class E_INDEX_TYPE;
//   enum class E_VERTEX_TYPE;
//   enum class E_TEXTURE_CREATION_FLAG;
//   enum class E_TEXTURE_TYPE;

  //! Special render targets, which usually map to dedicated hardware
  /** These render targets (besides 0 and 1) need not be supported by gfx cards */
  enum class E_RENDER_TARGET_TYPE
  {
    //! Render target is the main color frame buffer
    FRAME_BUFFER= 0,
    //! Render target is a render texture
    RENDER_TEXTURE,
    //! Multi-Render target textures
    MULTI_RENDER_TEXTURES,
    //! Render target is the main color frame buffer
    STEREO_LEFT_BUFFER,
    //! Render target is the right color buffer (left is the main buffer)
    STEREO_RIGHT_BUFFER,
    //! Render to both stereo buffers at once
    STEREO_BOTH_BUFFERS,
    //! Auxiliary buffer 0
    AUX_BUFFER0,
    //! Auxiliary buffer 1
    AUX_BUFFER1,
    //! Auxiliary buffer 2
    AUX_BUFFER2,
    //! Auxiliary buffer 3
    AUX_BUFFER3,
    //! Auxiliary buffer 4
    AUX_BUFFER4
  };

  //! enum class for the types of fog distributions to choose from
  enum class E_FOG_TYPE
  {
    EXP,
    LINEAR,
    EXP2
  };

  enum class E_SHADER_TYPE;

  class IVideoDriver
  {
  public:
    virtual const std::string& getVendorName() const = 0;

    virtual void beginPass(RenderPassHandle pass) = 0;
    virtual void render() = 0;
    virtual void submit() = 0;
    virtual void present(TextureHandle texture = NULL_GPU_RESOURCE_HANDLE) = 0;
    virtual void endPass(bool render = true) = 0;

    virtual SRenderPass createRenderPass() const = 0;
    virtual SGPUResource::HandleType createResource(SRenderPass&& pass) = 0;

    virtual SPipeline createPipeline() const = 0;
    virtual SGPUResource::HandleType createResource(SPipeline&& pipeline) = 0;

    virtual STexture createTexture() = 0;
    virtual TextureHandle createTexture(STexture&& texture) = 0;
    virtual TextureHandle createTexture(const std::string& path) = 0;
    virtual TextureHandle createTexture(unsigned char* data, std::size_t size) = 0;
    virtual void bindTexture(TextureHandle texture, int binding) = 0;
  
    virtual SShader createShader() const = 0;
    virtual SGPUResource::HandleType createResource(SShader&& shader) = 0;
    virtual SShaderUniform createShaderUniform() const = 0;
    virtual SGPUResource::HandleType createResource(SShaderUniform&& shader) = 0;

    virtual void updateShaderUniform(ShaderUniformHandle uniform, void* data) = 0;
    virtual void bindShaderUniform(ShaderUniformHandle uniform, int binding) = 0;

    virtual void addShader(SShader& shader, E_SHADER_TYPE type, std::string&& source) const = 0;
    virtual void addShaderFromFile(SShader& shader, E_SHADER_TYPE type, const std::string& path) const = 0;

    virtual void createGPUMeshBuffer(scene::IMeshBuffer& buffer, const RenderPassHandle pass) = 0;

    //! Get window's width
    virtual std::uint32_t getWidth() const = 0;

    //! Get window's height
    virtual std::uint32_t getHeight() const = 0;

    virtual void setSceneManager(const std::shared_ptr<scene::ISceneManager>& smgr) = 0;

    //! Copy texture content to another texture
    /**
      \param srcTex source texture handle
      \param dstTex destination texture handle
      \param srcOffset range of source texture to copy, default is (width, height) of srcTex
      \param dstOffset range of destination texture to be written, default is (width, height) of dstTex
    */
    virtual void copyTexture(TextureHandle srcTex, TextureHandle dstTex,
      const glm::ivec2& srcOffset = {}, const glm::ivec2& dstOffset = {}, const glm::ivec2& size = {}) = 0;

    //! Blit texture content to another texture
    /** Format conversion may perform
      \param srcTex source texture handle
      \param dstTex destination texture handle
      \param srcOffset range of source texture to blit, default is (width, height) of srcTex
      \param dstOffset range of destination texture to be written, default is (width, height) of dstTex
    */
    virtual void blitTexture(TextureHandle srcTex, TextureHandle dstTex,
      const glm::ivec2& srcOffset = {}, const glm::ivec2& dstOffset = {}) = 0;
  };

} // namespace video
} // namespace saga


#endif
