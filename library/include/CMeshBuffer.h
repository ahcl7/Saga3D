// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __CMESH_BUFFER_H_INCLUDED__
#define __CMESH_BUFFER_H_INCLUDED__

#include "EPrimitiveTypes.h"
#include "IMeshBuffer.h"
#include <vector>
#include <algorithm>

namespace saga
{
namespace scene
{
  //! Template implementation of the IMeshBuffer interface
  class CMeshBuffer : public IMeshBuffer
  {
  public:
    //! Default constructor for empty meshbuffer
    CMeshBuffer()
      :  ID(++RootID), PrimitiveType(E_PRIMITIVE_TYPE::TRIANGLES)
    {

    }

    virtual ~CMeshBuffer() {}

    virtual std::uint64_t getID() const override { return ID; }

    virtual void buildBuffer(const video::RenderPassHandle pass, const video::SPipelineLayout& layout) override
    {
      auto it = std::find_if(GPUBuffers.begin(), GPUBuffers.end(), [pass] (const auto& passBuffer) {
        return passBuffer.Pass == pass;
      });
      if (it == GPUBuffers.end())
      {
        GPUBuffers.push_back({ pass });
        auto& buffer = GPUBuffers.back().Buffer;
        for (std::size_t i = 0 ; i < getVertexCount(); ++i)
        {
          for (const auto& attr : layout.Attributes)
          {
            switch (attr.Type)
            {
              case video::E_ATTRIBUTE_TYPE::INVALID: continue;
              case video::E_ATTRIBUTE_TYPE::POSITION:
              {
                buffer.push_back(PositionBuffer.at(i).x);
                buffer.push_back(PositionBuffer.at(i).y);
                buffer.push_back(PositionBuffer.at(i).z);
              } break;

              case video::E_ATTRIBUTE_TYPE::NORMAL:
              {
                buffer.push_back(NormalBuffer.at(i).x);
                buffer.push_back(NormalBuffer.at(i).y);
                buffer.push_back(NormalBuffer.at(i).z);
              } break;

              case video::E_ATTRIBUTE_TYPE::COLOR:
              {
                buffer.push_back(ColorBuffer.at(i).x);
                buffer.push_back(ColorBuffer.at(i).y);
                buffer.push_back(ColorBuffer.at(i).z);
              } break;

              case video::E_ATTRIBUTE_TYPE::TEXTURE_COORDINATE:
              {
                buffer.push_back(TCoordBuffer.at(i).x);
                buffer.push_back(TCoordBuffer.at(i).y);
              } break;

              case video::E_ATTRIBUTE_TYPE::TANGENT:
              {
                buffer.push_back(TangentBuffer.at(i).x);
                buffer.push_back(TangentBuffer.at(i).y);
                buffer.push_back(TangentBuffer.at(i).z);
              } break;

              case video::E_ATTRIBUTE_TYPE::BITANGENT:
              {
                buffer.push_back(BiTangentBuffer.at(i).x);
                buffer.push_back(BiTangentBuffer.at(i).y);
                buffer.push_back(BiTangentBuffer.at(i).z);
              } break;
            }
          }
        }
      }
    }

    //! Get pointer to vertices
    /** \return Pointer to vertices. */
    virtual void* getVertices() override
    {
      return PositionBuffer.data();
    }

    //! Get pointer to GPU staging buffer
    /** \return Pointer to staging buffer. */
    virtual void* getData(const video::RenderPassHandle pass) override
    {
      auto it = std::find_if(GPUBuffers.begin(), GPUBuffers.end(), [pass] (const auto& passBuffer) {
        return passBuffer.Pass == pass;
      });
      if (it != GPUBuffers.end())
        return it->Buffer.data();
      else
        return nullptr;
    }

    //! Get size of GPU staging buffer
    /** \return Size of staging buffer. */
    virtual std::size_t getDataSize(const video::RenderPassHandle pass) override
    {
      auto it = std::find_if(GPUBuffers.begin(), GPUBuffers.end(), [pass] (const auto& passBuffer) {
        return passBuffer.Pass == pass;
      });
      if (it != GPUBuffers.end())
        return it->Buffer.size() * sizeof(float);
      else
        return 0;
    }

    //! Get number of vertices
    /** \return Number of vertices. */
    virtual std::uint32_t getVertexCount() const override
    {
      return PositionBuffer.size();
    }

    //! Get pointer to indices
    /** \return Pointer to indices. */
    virtual const std::uint32_t* getIndices() const override
    {
      return Indices.data();
    }

    //! Get pointer to indices
    /** \return Pointer to indices. */
    virtual std::uint32_t* getIndices() override
    {
      return Indices.data();
    }

    //! Get number of indices
    /** \return Number of indices. */
    virtual std::uint32_t getIndexCount() const override
    {
      return Indices.size();
    }

    //! Get the axis aligned bounding box
    /** \return Axis aligned bounding box of this buffer. */
    virtual const core::aabbox3d<float>& getBoundingBox() const override
    {
      return BoundingBox;
    }

    //! Set the axis aligned bounding box
    /** \param box New axis aligned bounding box for this buffer. */
    //! set user axis aligned bounding box
    virtual void setBoundingBox(const core::aabbox3df& box) override
    {
      BoundingBox = box;
    }

    //! Recalculate the bounding box.
    /** should be called if the mesh changed. */
    virtual void recalculateBoundingBox() override
    {
      if (!PositionBuffer.empty())
      {
        BoundingBox.reset(PositionBuffer[0]);
        const std::uint32_t vsize = PositionBuffer.size();
        for (std::uint32_t i=1; i < vsize; ++i)
          BoundingBox.addInternalPoint(PositionBuffer[i]);
      }
      else
        BoundingBox.reset(0,0,0);
    }

    //! returns position of vertex i
    virtual const glm::vec3& getPosition(std::uint32_t i) const override
    {
      return PositionBuffer[i];
    }

    //! returns normal of vertex i
    virtual const glm::vec3& getNormal(std::uint32_t i) const override
    {
      return NormalBuffer[i];
    }

    //! returns texture coord of vertex i
    virtual const glm::vec2& getTCoords(std::uint32_t i) const override
    {
      return TCoordBuffer[i];
    }

    //! returns tangent of vertex i
    virtual const glm::vec3& getTangent(std::uint32_t i) const override
    {
      return TangentBuffer[i];
    }

    //! returns bi-tangent of vertex i
    virtual const glm::vec3& getBiTangent(std::uint32_t i) const override
    {
      return BiTangentBuffer[i];
    }

    //! Append the vertices and indices to the current buffer
    /** Only works for compatible types, i.e. either the same type
    or the main buffer is of standard type. Otherwise, behavior is
    undefined.
    */
    virtual void append(
      std::vector<S3DVertex>&& vertices,
      std::uint32_t numPositionBuffer,
      std::vector<uint32_t>&& indices,
      std::uint32_t numIndices) override
    {
      const std::uint32_t vertexCount = getVertexCount();
      std::uint32_t i;

      for (i = 0; i < numPositionBuffer; ++i)
      {
        PositionBuffer.push_back(std::move(vertices[i].Pos));
        NormalBuffer.push_back(std::move(vertices[i].Normal));
        ColorBuffer.push_back(std::move(vertices[i].Color));
        TCoordBuffer.push_back(std::move(vertices[i].TCoords));
        TangentBuffer.push_back(std::move(vertices[i].Tangent));
        BiTangentBuffer.push_back(std::move(vertices[i].BiTangent));
        BoundingBox.addInternalPoint(PositionBuffer.back());
      }

      for (i = 0; i < numIndices; ++i)
      {
        Indices.push_back(indices[i]+vertexCount);
      }
    }

    //! Append the meshbuffer to the current buffer
    /** Only works for compatible types, i.e. either the same type
    or the main buffer is of standard type. Otherwise, behavior is
    undefined.
    \param other Meshbuffer to be appended to this one.
    */
    virtual void append(const IMeshBuffer* const other) override
    {
      /*
      if (this==other)
        return;

      const std::uint32_t vertexCount = getVertexCount();
      std::uint32_t i;

      PositionBuffer.reallocate(vertexCount+other->getVertexCount());
      for (i = 0; i < other->getVertexCount(); ++i)
      {
        PositionBuffer.push_back(reinterpret_cast<const T*>(other->getPositionBuffer())[i]);
      }

      Indices.reallocate(getIndexCount()+other->getIndexCount());
      for (i = 0; i < other->getIndexCount(); ++i)
      {
        Indices.push_back(other->getIndices()[i]+vertexCount);
      }
      BoundingBox.addInternalBox(other->getBoundingBox());
      */
    }

    //! Describe what kind of primitive geometry is used by the meshbuffer
    virtual void setPrimitiveType(scene::E_PRIMITIVE_TYPE type) override
    {
      PrimitiveType = type;
    }

    //! Get the kind of primitive geometry which is used by the meshbuffer
    virtual scene::E_PRIMITIVE_TYPE getPrimitiveType() const override
    {
      return PrimitiveType;
    }

    static std::uint64_t RootID;
    const std::uint64_t ID;

    //! Vertex position buffer of this mesh buffer
    std::vector<glm::vec3> PositionBuffer;
    //! Vertex normal buffer of this mesh buffer
    std::vector<glm::vec3> NormalBuffer;
    //! Vertex color buffer of this mesh buffer
    std::vector<glm::vec4> ColorBuffer;
    //! Vertex texture coordinate buffer of this mesh buffer
    std::vector<glm::vec2> TCoordBuffer;
    //! Vertex tangent buffer of this mesh buffer
    std::vector<glm::vec3> TangentBuffer;
    //! Vertex bitangent buffer of this mesh buffer
    std::vector<glm::vec3> BiTangentBuffer;

    struct PassBuffer
    {
      video::RenderPassHandle Pass;
      std::vector<float> Buffer;
    };

    //! GPU staging buffer
    std::vector<PassBuffer> GPUBuffers;

    //! Indices into the vertices of this buffer.
    std::vector<std::uint32_t> Indices;
    //! Bounding box of this meshbuffer.
    core::aabbox3d<float> BoundingBox;
    //! Primitive type used for rendering (triangles, lines, ...)
    scene::E_PRIMITIVE_TYPE PrimitiveType;
  };

} // namespace scene
} // namespace saga

#endif


