// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __E_SCENE_NODE_ANIMATOR_TYPES_H_INCLUDED__
#define __E_SCENE_NODE_ANIMATOR_TYPES_H_INCLUDED__

namespace saga
{
namespace scene
{

  //! An enumeration for all types of built-in scene node animators
  enum class E_SCENE_NODE_ANIMATOR_TYPE
  {
    //! Fly circle scene node animator
    FLY_CIRCLE,

    //! Fly straight scene node animator
    FLY_STRAIGHT,

    //! Follow spline scene node animator
    FOLLOW_SPLINE,

    //! Rotation scene node animator
    ROTATION,

    //! Texture scene node animator
    TEXTURE,

    //! Deletion scene node animator
    DELETION,

    //! Collision response scene node animator
    COLLISION_RESPONSE,

    //! FPS camera animator
    CAMERA_FPS,

    //! Maya camera animator
    CAMERA_MAYA,

    //! Amount of built-in scene node animators
    COUNT,

    //! Unknown scene node animator
    UNKNOWN,
  };

} // namespace scene
} // namespace saga

#endif // __E_SCENE_NODE_ANIMATOR_TYPES_H_INCLUDED__
