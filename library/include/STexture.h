#ifndef __STEXTURE_H_INCLUDED__
#define __STEXTURE_H_INCLUDED__

#include "GraphicsConstants.h"
#include "SGPUResource.h"
#include "EFilterTypes.h"
#include "EMipMapModes.h"
#include "ETextureTypes.h"
#include "ESamplerAddressModes.h"
#include "EPixelFormat.h"
#include <cstdint>
#include <memory>
#include <vector>
#include <array>

namespace saga
{
namespace video 
{
  struct STextureContent
  {
    std::vector<unsigned char> Data;
  };

  struct STexture : public SGPUResource
  {
    E_TEXTURE_TYPE Type = E_TEXTURE_TYPE::CUBE_MAP;
    E_PIXEL_FORMAT Format;
    E_FILTER_TYPE MinFilter = E_FILTER_TYPE::LINEAR;
    E_FILTER_TYPE MagFilter = E_FILTER_TYPE::LINEAR;
    E_MIPMAP_MODE MipMapMode;
    E_SAMPLER_ADDRESS_MODE AddressModeU = E_SAMPLER_ADDRESS_MODE::CLAMP_TO_EDGE;
    E_SAMPLER_ADDRESS_MODE AddressModeV = E_SAMPLER_ADDRESS_MODE::CLAMP_TO_EDGE;
    E_SAMPLER_ADDRESS_MODE AddressModeW = E_SAMPLER_ADDRESS_MODE::CLAMP_TO_EDGE;
    int Width = 0;
    int Height = 0;
    bool IsRenderTarget = false;
    bool IsDepthAttachment = false;
    int MipMapCount = 1;
    int SampleCount = 1;
    float MinLOD = 0.f;
    float MaxLOD;
    std::uint32_t MaxAnisotropy;
    std::array<std::array<STextureContent, MAX_MIPMAPS>, CUBE_FACE_COUNT> Contents;
  };

  using TextureHandle = SGPUResource::HandleType;

} // namespace scene
} // namespace saga

#endif // __STEXTURE_H_INCLUDED__

