// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_PARTICLE_CYLINDER_EMITTER_H_INCLUDED__
#define __I_PARTICLE_CYLINDER_EMITTER_H_INCLUDED__

#include "IParticleEmitter.h"

namespace saga
{
namespace scene
{

//! A particle emitter which emits from a cylindrically shaped space.
class IParticleCylinderEmitter : public IParticleEmitter
{
public:

  //! Set the center of the radius for the cylinder, at one end of the cylinder
  virtual void setCenter(const glm::vec3& center) = 0;

  //! Set the normal of the cylinder
  virtual void setNormal(const glm::vec3& normal) = 0;

  //! Set the radius of the cylinder
  virtual void setRadius(float radius) = 0;

  //! Set the length of the cylinder
  virtual void setLength(float length) = 0;

  //! Set whether or not to draw points inside the cylinder
  virtual void setOutlineOnly(bool outlineOnly = true) = 0;

  //! Get the center of the cylinder
  virtual const glm::vec3& getCenter() const = 0;

  //! Get the normal of the cylinder
  virtual const glm::vec3& getNormal() const = 0;

  //! Get the radius of the cylinder
  virtual float getRadius() const = 0;

  //! Get the center of the cylinder
  virtual float getLength() const = 0;

  //! Get whether or not to draw points inside the cylinder
  virtual bool getOutlineOnly() const = 0;

  //! Get emitter type
  virtual E_PARTICLE_EMITTER_TYPE getType() const { return EPET_CYLINDER; }
};

} // namespace scene
} // namespace saga


#endif

