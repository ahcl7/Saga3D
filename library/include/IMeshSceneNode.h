// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_MESH_SCENE_NODE_H_INCLUDED__
#define __I_MESH_SCENE_NODE_H_INCLUDED__

#include "ISceneNode.h"

namespace saga
{
namespace scene
{

class IMesh;


//! A scene node displaying a static mesh
class IMeshSceneNode : public ISceneNode
{
public:

  //! Constructor
  /** Use setMesh() to set the mesh to display.
  */
  IMeshSceneNode(const std::shared_ptr<ISceneNode>& parent, const std::shared_ptr<ISceneManager>& mgr,
    const glm::vec3& position = glm::vec3(0,0,0),
    const glm::vec3& rotation = glm::vec3(0,0,0),
    const glm::vec3& scale = glm::vec3(1,1,1))
    : ISceneNode(parent, mgr, position, rotation, scale) {}

  //! Sets a new mesh to display
  /** \param mesh Mesh to display. */
  virtual void setMesh(const std::shared_ptr<IMesh>& mesh) = 0;

  //! Get the currently defined mesh for display.
  /** \return Pointer to mesh which is displayed by this node. */
  virtual const std::shared_ptr<IMesh>& getMesh() const = 0;

  virtual void OnRegisterSceneNode(video::RenderPassHandle pass) = 0;
};

} // namespace scene
} // namespace saga


#endif

