// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_SKIN_MESH_BUFFER_H_INCLUDED__
#define __I_SKIN_MESH_BUFFER_H_INCLUDED__

#include "IMeshBuffer.h"
#include "S3DVertex.h"


namespace saga
{
namespace scene
{


//! A mesh buffer able to choose between S3DVertex2TCoords, S3DVertex and S3DVertexTangents at runtime
struct SSkinMeshBuffer : public IMeshBuffer
{
  //! Default constructor
  SSkinMeshBuffer(scene::E_VERTEX_TYPE vt=scene::E_VERTEX_TYPE::STANDARD) :
    ChangedID_Vertex(1), ChangedID_Index(1), VertexType(vt),
    PrimitiveType(E_PRIMITIVE_TYPE::TRIANGLES),
    MappingHint_Vertex(E_HARDWARE_MAPPING::NEVER), MappingHint_Index(E_HARDWARE_MAPPING::NEVER),
    BoundingBoxNeedsRecalculated(true)
  {
    #ifdef _DEBUG
    setDebugName("SSkinMeshBuffer");
    #endif
  }

  // //! Get Material of this buffer.
  // virtual const video::SMaterial& getMaterial() const
  // {
  //   return Material;
  // }

  // //! Get Material of this buffer.
  // virtual video::SMaterial& getMaterial()
  // {
  //   return Material;
  // }

  //! Get standard vertex at given index
  virtual video::S3DVertex *getVertex(std::uint32_t index)
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return (video::S3DVertex*)&Vertices_2TCoords[index];
      case scene::E_VERTEX_TYPE::TANGENTS:
        return (video::S3DVertex*)&Vertices_Tangents[index];
      default:
        return &Vertices_Standard[index];
    }
  }

  //! Get pointer to vertex array
  virtual const void* getVertices() const
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return Vertices_2TCoords.data();
      case scene::E_VERTEX_TYPE::TANGENTS:
        return Vertices_Tangents.data();
      default:
        return Vertices_Standard.data();
    }
  }

  //! Get pointer to vertex array
  virtual void* getVertices()
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return Vertices_2TCoords.data();
      case scene::E_VERTEX_TYPE::TANGENTS:
        return Vertices_Tangents.data();
      default:
        return Vertices_Standard.data();
    }
  }

  //! Get vertex count
  virtual std::uint32_t getVertexCount() const
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return Vertices_2TCoords.size();
      case scene::E_VERTEX_TYPE::TANGENTS:
        return Vertices_Tangents.size();
      default:
        return Vertices_Standard.size();
    }
  }

  //! Get type of index data which is stored in this meshbuffer.
  /** \return Index type of this buffer. */
  virtual scene::E_INDEX_TYPE getIndexType() const
  {
    return scene::E_INDEX_TYPE::BITS_16;
  }

  //! Get pointer to index array
  virtual const std::uint16_t* getIndices() const
  {
    return Indices.data();
  }

  //! Get pointer to index array
  virtual std::uint16_t* getIndices()
  {
    return Indices.data();
  }

  //! Get index count
  virtual std::uint32_t getIndexCount() const
  {
    return Indices.size();
  }

  //! Get bounding box
  virtual const core::aabbox3d<float>& getBoundingBox() const
  {
    return BoundingBox;
  }

  //! Set bounding box
  virtual void setBoundingBox(const core::aabbox3df& box)
  {
    BoundingBox = box;
  }

  //! Recalculate bounding box
  virtual void recalculateBoundingBox()
  {
    if(!BoundingBoxNeedsRecalculated)
      return;

    BoundingBoxNeedsRecalculated = false;

    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::STANDARD:
      {
        if (Vertices_Standard.empty())
          BoundingBox.reset(0,0,0);
        else
        {
          BoundingBox.reset(Vertices_Standard[0].Pos);
          for (std::uint32_t i=1; i < Vertices_Standard.size(); ++i)
            BoundingBox.addInternalPoint(Vertices_Standard[i].Pos);
        }
        break;
      }
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
      {
        if (Vertices_2TCoords.empty())
          BoundingBox.reset(0,0,0);
        else
        {
          BoundingBox.reset(Vertices_2TCoords[0].Pos);
          for (std::uint32_t i=1; i < Vertices_2TCoords.size(); ++i)
            BoundingBox.addInternalPoint(Vertices_2TCoords[i].Pos);
        }
        break;
      }
      case scene::E_VERTEX_TYPE::TANGENTS:
      {
        if (Vertices_Tangents.empty())
          BoundingBox.reset(0,0,0);
        else
        {
          BoundingBox.reset(Vertices_Tangents[0].Pos);
          for (std::uint32_t i=1; i < Vertices_Tangents.size(); ++i)
            BoundingBox.addInternalPoint(Vertices_Tangents[i].Pos);
        }
        break;
      }
    }
  }

  //! Get vertex type
  virtual scene::E_VERTEX_TYPE getVertexType() const
  {
    return VertexType;
  }

  //! Convert to 2tcoords vertex type
  virtual void convertTo2TCoords()
  {
    if (VertexType==scene::E_VERTEX_TYPE::STANDARD)
    {
      for(std::uint32_t n= 0;n<Vertices_Standard.size();++n)
      {
        video::S3DVertex2TCoords Vertex;
        Vertex.Color=Vertices_Standard[n].Color;
        Vertex.Pos=Vertices_Standard[n].Pos;
        Vertex.Normal=Vertices_Standard[n].Normal;
        Vertex.TCoords=Vertices_Standard[n].TCoords;
        Vertices_2TCoords.push_back(Vertex);
      }
      Vertices_Standard.clear();
      VertexType=scene::E_VERTEX_TYPE::TWO_TCOORDS;
    }
  }

  //! Convert to tangents vertex type
  virtual void convertToTangents()
  {
    if (VertexType==scene::E_VERTEX_TYPE::STANDARD)
    {
      for(std::uint32_t n= 0;n<Vertices_Standard.size();++n)
      {
        video::S3DVertexTangents Vertex;
        Vertex.Color=Vertices_Standard[n].Color;
        Vertex.Pos=Vertices_Standard[n].Pos;
        Vertex.Normal=Vertices_Standard[n].Normal;
        Vertex.TCoords=Vertices_Standard[n].TCoords;
        Vertices_Tangents.push_back(Vertex);
      }
      Vertices_Standard.clear();
      VertexType=scene::E_VERTEX_TYPE::TANGENTS;
    }
    else if (VertexType==scene::E_VERTEX_TYPE::TWO_TCOORDS)
    {
      for(std::uint32_t n= 0;n<Vertices_2TCoords.size();++n)
      {
        video::S3DVertexTangents Vertex;
        Vertex.Color=Vertices_2TCoords[n].Color;
        Vertex.Pos=Vertices_2TCoords[n].Pos;
        Vertex.Normal=Vertices_2TCoords[n].Normal;
        Vertex.TCoords=Vertices_2TCoords[n].TCoords;
        Vertices_Tangents.push_back(Vertex);
      }
      Vertices_2TCoords.clear();
      VertexType=scene::E_VERTEX_TYPE::TANGENTS;
    }
  }

  //! returns position of vertex i
  virtual const glm::vec3& getPosition(std::uint32_t i) const
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return Vertices_2TCoords[i].Pos;
      case scene::E_VERTEX_TYPE::TANGENTS:
        return Vertices_Tangents[i].Pos;
      default:
        return Vertices_Standard[i].Pos;
    }
  }

  //! returns position of vertex i
  virtual glm::vec3& getPosition(std::uint32_t i)
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return Vertices_2TCoords[i].Pos;
      case scene::E_VERTEX_TYPE::TANGENTS:
        return Vertices_Tangents[i].Pos;
      default:
        return Vertices_Standard[i].Pos;
    }
  }

  //! returns normal of vertex i
  virtual const glm::vec3& getNormal(std::uint32_t i) const
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return Vertices_2TCoords[i].Normal;
      case scene::E_VERTEX_TYPE::TANGENTS:
        return Vertices_Tangents[i].Normal;
      default:
        return Vertices_Standard[i].Normal;
    }
  }

  //! returns normal of vertex i
  virtual glm::vec3& getNormal(std::uint32_t i)
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return Vertices_2TCoords[i].Normal;
      case scene::E_VERTEX_TYPE::TANGENTS:
        return Vertices_Tangents[i].Normal;
      default:
        return Vertices_Standard[i].Normal;
    }
  }

  //! returns texture coords of vertex i
  virtual const glm::vec2& getTCoords(std::uint32_t i) const
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return Vertices_2TCoords[i].TCoords;
      case scene::E_VERTEX_TYPE::TANGENTS:
        return Vertices_Tangents[i].TCoords;
      default:
        return Vertices_Standard[i].TCoords;
    }
  }

  //! returns texture coords of vertex i
  virtual glm::vec2& getTCoords(std::uint32_t i)
  {
    switch (VertexType)
    {
      case scene::E_VERTEX_TYPE::TWO_TCOORDS:
        return Vertices_2TCoords[i].TCoords;
      case scene::E_VERTEX_TYPE::TANGENTS:
        return Vertices_Tangents[i].TCoords;
      default:
        return Vertices_Standard[i].TCoords;
    }
  }

  //! append the vertices and indices to the current buffer
  virtual void append(const void* const vertices, std::uint32_t numVertices, const std::uint16_t* const indices, std::uint32_t numIndices) {}

  //! append the meshbuffer to the current buffer
  virtual void append(const IMeshBuffer* const other) {}

  //! get the current hardware mapping hint for vertex buffers
  virtual E_HARDWARE_MAPPING getHardwareMappingHint_Vertex() const
  {
    return MappingHint_Vertex;
  }

  //! get the current hardware mapping hint for index buffers
  virtual E_HARDWARE_MAPPING getHardwareMappingHint_Index() const
  {
    return MappingHint_Index;
  }

  //! set the hardware mapping hint, for driver
  virtual void setHardwareMappingHint(E_HARDWARE_MAPPING NewMappingHint, E_BUFFER_TYPE Buffer=E_BUFFER_TYPE::VERTEX_AND_INDEX)
  {
    if (Buffer==E_BUFFER_TYPE::VERTEX)
      MappingHint_Vertex=NewMappingHint;
    else if (Buffer==E_BUFFER_TYPE::INDEX)
      MappingHint_Index=NewMappingHint;
    else if (Buffer==E_BUFFER_TYPE::VERTEX_AND_INDEX)
    {
      MappingHint_Vertex=NewMappingHint;
      MappingHint_Index=NewMappingHint;
    }
  }

  //! Describe what kind of primitive geometry is used by the meshbuffer
  virtual void setPrimitiveType(scene::E_PRIMITIVE_TYPE type)
  {
    PrimitiveType = type;
  }

  //! Get the kind of primitive geometry which is used by the meshbuffer
  virtual scene::E_PRIMITIVE_TYPE getPrimitiveType() const
  {
    return PrimitiveType;
  }

  //! flags the mesh as changed, reloads hardware buffers
  virtual void setDirty(E_BUFFER_TYPE Buffer=E_BUFFER_TYPE::VERTEX_AND_INDEX)
  {
    if (Buffer==E_BUFFER_TYPE::VERTEX_AND_INDEX || Buffer==E_BUFFER_TYPE::VERTEX)
      ++ChangedID_Vertex;
    if (Buffer==E_BUFFER_TYPE::VERTEX_AND_INDEX || Buffer==E_BUFFER_TYPE::INDEX)
      ++ChangedID_Index;
  }

  virtual std::uint32_t getChangedID_Vertex() const {return ChangedID_Vertex;}

  virtual std::uint32_t getChangedID_Index() const {return ChangedID_Index;}

  //! Call this after changing the positions of any vertex.
  void boundingBoxNeedsRecalculated(void) { BoundingBoxNeedsRecalculated = true; }

  std::vector<video::S3DVertexTangents> Vertices_Tangents;
  std::vector<video::S3DVertex2TCoords> Vertices_2TCoords;
  std::vector<video::S3DVertex> Vertices_Standard;
  std::vector<std::uint16_t> Indices;

  std::uint32_t ChangedID_Vertex;
  std::uint32_t ChangedID_Index;

  //ISkinnedMesh::SJoint *AttachedJoint;
  glm::mat4 Transformation;

  //video::SMaterial Material;
  scene::E_VERTEX_TYPE VertexType;

  core::aabbox3d<float> BoundingBox;

  //! Primitive type used for rendering (triangles, lines, ...)
  scene::E_PRIMITIVE_TYPE PrimitiveType;

  // hardware mapping hint
  E_HARDWARE_MAPPING MappingHint_Vertex:3;
  E_HARDWARE_MAPPING MappingHint_Index:3;

  bool BoundingBoxNeedsRecalculated:1;
};


} // namespace scene
} // namespace saga

#endif

