#ifndef __E_PIPELINE_TYPES_H_INCLUDED__
#define __E_PIPELINE_TYPES_H_INCLUDED__

namespace saga
{
namespace scene
{

  enum class E_PIPELINE_TYPE
  {
    GRAPHICS,
    COMPUTE    
  };

} // namespace scene
} // namespace saga

#endif

