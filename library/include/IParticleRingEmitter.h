// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_PARTICLE_RING_EMITTER_H_INCLUDED__
#define __I_PARTICLE_RING_EMITTER_H_INCLUDED__

#include "IParticleEmitter.h"

namespace saga
{
namespace scene
{

//! A particle emitter which emits particles along a ring shaped area.
class IParticleRingEmitter : public IParticleEmitter
{
public:

  //! Set the center of the ring
  virtual void setCenter(const glm::vec3& center) = 0;

  //! Set the radius of the ring
  virtual void setRadius(float radius) = 0;

  //! Set the thickness of the ring
  virtual void setRingThickness(float ringThickness) = 0;

  //! Get the center of the ring
  virtual const glm::vec3& getCenter() const = 0;

  //! Get the radius of the ring
  virtual float getRadius() const = 0;

  //! Get the thickness of the ring
  virtual float getRingThickness() const = 0;

  //! Get emitter type
  virtual E_PARTICLE_EMITTER_TYPE getType() const { return EPET_RING; }
};

} // namespace scene
} // namespace saga


#endif

