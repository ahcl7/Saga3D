// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __I_MESH_H_INCLUDED__
#define __I_MESH_H_INCLUDED__


#include "aabbox3d.h"
#include <memory>

namespace saga
{
namespace scene
{
  class IMeshBuffer;

  //! Class which holds the geometry of an object.
  /** An IMesh is nothing more than a collection of some mesh buffers
  (IMeshBuffer). SMesh is a simple implementation of an IMesh.
  A mesh is usually added to an IMeshSceneNode in order to be rendered.
  */
  class IMesh
  {
  public:

    //! Get the amount of mesh buffers.
    /** \return Amount of mesh buffers (IMeshBuffer) in this mesh. */
    virtual std::uint32_t getMeshBufferCount() const = 0;

    //! Get pointer to a mesh buffer.
    /** \param nr: Zero based index of the mesh buffer. The maximum value is
    getMeshBufferCount() - 1;
    \return Pointer to the mesh buffer or 0 if there is no such
    mesh buffer. */
    virtual IMeshBuffer& getMeshBuffer(std::uint32_t nr = 0) const = 0;

    //! Get an axis aligned bounding box of the mesh.
    /** \return Bounding box of this mesh. */
    virtual const core::aabbox3d<float>& getBoundingBox() const = 0;

    //! Set user-defined axis aligned bounding box
    /** \param box New bounding box to use for the mesh. */
    virtual void setBoundingBox(const core::aabbox3df& box) = 0;
  };

} // namespace scene
} // namespace saga

#endif

