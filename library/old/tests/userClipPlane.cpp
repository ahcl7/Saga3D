#include "testUtils.h"

using namespace irr;

static bool withSphere(video::E_DRIVER_TYPE type)
{
  IrrlichtDevice* device = createDevice(type, core::dimension2d<u32>(160, 120));

  if (device == 0)
    return true;

  video::IVideoDriver* driver = device->getVideoDriver();
  scene::ISceneManager* smgr = device->getSceneManager();

  stabilizeScreenBackground(driver);

  driver->setClipPlane(0, core::plane3df(glm::vec3(0,18,0), glm::vec3(0,-1,0)), true);
  smgr->addLightSceneNode(0, glm::vec3(30,30,50));
  // create first sphere
  scene::ISceneNode* sphere = smgr->addMeshSceneNode(smgr->addSphereMesh("sphere", 10, 64, 64));

  if (sphere)
  {
    sphere->setPosition(glm::vec3(0,10,0));
    sphere->setMaterialFlag(video::E_MATERIAL_FLAG::BACK_FACE_CULLING, false);
  }

  sphere = smgr->addMeshSceneNode(smgr->addHillPlaneMesh("mesh", glm::vec2(10,10), glm::uvec2(10,10)));
  sphere->setPosition(glm::vec3(0,10,0));

  smgr->addCameraSceneNode(0, glm::vec3(-25,30,20), glm::vec3());

  driver->setClipPlane(1, core::plane3df(glm::vec3(0,2,0), glm::vec3(0,1,0)), true);
  driver->setClipPlane(2, core::plane3df(glm::vec3(8,0,0), glm::vec3(-1,0,0)));

  device->run();
//  while(device->run())
  {
  driver->beginScene(video::E_CLEAR_BUFFER_FLAG::COLOR | video::E_CLEAR_BUFFER_FLAG::DEPTH, video::SColor(255,113,113,133));
  driver->setClipPlane(3, core::plane3df(glm::vec3(-8,0,0), glm::vec3(1,0,0)), true);
  driver->setClipPlane(4, core::plane3df(glm::vec3(0,0,8), glm::vec3(0,0,-1)));
  driver->setClipPlane(5, core::plane3df(glm::vec3(0,0,-8), glm::vec3(0,0,1)));
  driver->enableClipPlane(2, true);
  driver->enableClipPlane(4, true);
  driver->enableClipPlane(5, true);
  smgr->drawAll();
  driver->enableClipPlane(1, false);
  driver->enableClipPlane(2, false);
  driver->enableClipPlane(4, false);
  driver->enableClipPlane(5, false);
  driver->setTransform(video::E_TRANSFORM_STATE::, core::IdentityMatrix);
  driver->setMaterial(video::IdentityMaterial);
  driver->draw3DLine(glm::vec3(), glm::vec3(0,0,50));
  driver->endScene();
  }
  bool result = takeScreenshotAndCompareAgainstReference(driver, "-ucpsphere.png");

  device->closeDevice();
  device->run();
  device->drop();
    return result;
}

bool userclipplane()
{
  bool result = true;
  TestWithAllHWDrivers(withSphere);
  return result;
}
