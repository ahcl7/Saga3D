// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h
// This device code is based on the original SDL device implementation
// contributed by Shane Parker (sirshane).

#ifndef __C_IRR_DEVICE_SDL_H_INCLUDED__
#define __C_IRR_DEVICE_SDL_H_INCLUDED__

#include "SagaDevice.h"
#include "CIrrDeviceStub.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

namespace saga
{
  class CIrrDeviceSDL : public CIrrDeviceStub
  {
  public:

    //! constructor
    CIrrDeviceSDL(const SIrrlichtCreationParameters& param);

    //! destructor
    virtual ~CIrrDeviceSDL();

    //! runs the device. Returns false if device wants to be deleted
    virtual bool run() override;

    //! pause execution temporarily
    virtual void yield() override;

    //! pause execution for a specified time
    virtual void sleep(std::uint32_t timeMs) override;

    //! sets the caption of the window
    virtual void setWindowCaption(const std::string& text) override;

    //! returns if window is active. if not, nothing need to be drawn
    virtual bool isWindowActive() const override;

    //! returns if window has focus.
    bool isWindowFocused() const override;

    //! returns if window is minimized.
    bool isWindowMinimized() const override;

    //! notifies the device that it should close itself
    virtual void closeDevice() override;

    //! Sets if the window should be resizable in windowed mode.
    virtual void setResizable(bool resize=false) override;

    //! Minimizes the window.
    virtual void minimizeWindow() override;

    //! Maximizes the window.
    virtual void maximizeWindow() override;

    //! Restores the window size.
    virtual void restoreWindow() override;

    //! Get the position of this window on screen
    virtual glm::ivec2 getWindowPosition() override;

    SDL_Window* getSDLWindow() const { return Window; }

    //! Get window's width
    virtual std::uint32_t getWidth() const override { return Width; }
    
    //! Get window's height
    virtual std::uint32_t getHeight() const override { return Height; }

    //! Resize the render window.
    /**  This will only work in windowed mode and is not yet supported on all systems.
    It does set the drawing/clientDC size of the window, the window decorations are added to that.
    You get the current window size with IVideoDriver::getScreenSize() (might be unified in future)
    */
    virtual void setWindowSize(const glm::uvec2& size) override {}

  private:

    //! create the driver
    void createDriver();
    bool createWindow();

    SDL_Surface* Screen;
    int SDL_Flags;

    std::uint32_t Width, Height;

    bool Resizable;
    bool WindowHasFocus;
    bool WindowMinimized;

    SDL_SysWMinfo Info;
    SDL_Window* Window;
};

} // namespace saga

#endif // __C_IRR_DEVICE_SDL_H_INCLUDED__

