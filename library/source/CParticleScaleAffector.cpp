// Copyright (C) 2010-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CParticleScaleAffector.h"

#ifdef _IRR_COMPILE_WITH_PARTICLES_



namespace saga
{
  namespace scene
  {
    CParticleScaleAffector::CParticleScaleAffector(const glm::vec2& scaleTo)
      : ScaleTo(scaleTo)
    {
      #ifdef _DEBUG
      setDebugName("CParticleScaleAffector");
      #endif
    }


    void CParticleScaleAffector::affect (std::uint32_t now, SParticle *particlearray, std::uint32_t count)
    {
      for(std::uint32_t i = 0;i < count;i++)
      {
        const std::uint32_t maxdiff = particlearray[i].endTime - particlearray[i].startTime;
        const std::uint32_t curdiff = now - particlearray[i].startTime;
        const float newscale = (float)curdiff/maxdiff;
        particlearray[i].size = particlearray[i].startSize+ScaleTo*newscale;
      }
    }


    void CParticleScaleAffector::serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const
    {
      out->addFloat("ScaleToWidth", ScaleTo.Width);
      out->addFloat("ScaleToHeight", ScaleTo.Height);
    }


    void CParticleScaleAffector::deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options)
    {
      ScaleTo.Width = in->getAttributeAsFloat("ScaleToWidth");
      ScaleTo.Height = in->getAttributeAsFloat("ScaleToHeight");
    }


    E_PARTICLE_AFFECTOR_TYPE CParticleScaleAffector::getType() const
    {
      return scene::EPAT_SCALE;
    }
  }
}

#endif // _IRR_COMPILE_WITH_PARTICLES_
