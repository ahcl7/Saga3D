// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CParticleGravityAffector.h"

#ifdef _IRR_COMPILE_WITH_PARTICLES_

#include "os.h"


namespace saga
{
namespace scene
{

//! constructor
CParticleGravityAffector::CParticleGravityAffector(
  const glm::vec3& gravity, std::uint32_t timeForceLost)
  : IParticleGravityAffector(), TimeForceLost(static_cast<float>(timeForceLost)), Gravity(gravity)
{
  #ifdef _DEBUG
  setDebugName("CParticleGravityAffector");
  #endif
}


//! Affects an array of particles.
void CParticleGravityAffector::affect(std::uint32_t now, SParticle* particlearray, std::uint32_t count)
{
  if (!Enabled)
    return;
  float d;

  for (std::uint32_t i = 0; i < count; ++i)
  {
    d = (now - particlearray[i].startTime) / TimeForceLost;
    if (d > 1.0f)
      d = 1.0f;
    if (d < 0.0f)
      d = 0.0f;
    d = 1.0f - d;

    particlearray[i].vector = particlearray[i].startVector.getInterpolated(Gravity, d);
  }
}

//! Writes attributes of the object.
void CParticleGravityAffector::serializeAttributes(io::IAttributes* out, io::SAttributeReadWriteOptions* options) const
{
  out->addVector3d("Gravity", Gravity);
  out->addFloat("TimeForceLost", TimeForceLost);
}


//! Reads attributes of the object.
void CParticleGravityAffector::deserializeAttributes(io::IAttributes* in, io::SAttributeReadWriteOptions* options)
{
  Gravity = in->getAttributeAsVector3d("Gravity");
  TimeForceLost = in->getAttributeAsFloat("TimeForceLost");
}



} // namespace scene
} // namespace saga

#endif // _IRR_COMPILE_WITH_PARTICLES_
