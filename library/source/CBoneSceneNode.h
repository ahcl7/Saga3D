// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_BONE_SCENE_NODE_H_INCLUDED__
#define __C_BONE_SCENE_NODE_H_INCLUDED__

// Used with SkinnedMesh and IAnimatedMeshSceneNode, for boned meshes
#include "IBoneSceneNode.h"

namespace saga
{
namespace scene
{

  class CBoneSceneNode : public IBoneSceneNode
  {
  public:

    //! constructor
    CBoneSceneNode(const std::shared_ptr<ISceneNode>& parent, const std::shared_ptr<ISceneManager>& mgr,
      std::uint32_t boneIndex = 0, const std::string& boneName= 0);

    //! Returns the index of the bone
    virtual std::uint32_t getBoneIndex() const override;

    //! Sets the animation mode of the bone. Returns true if successful.
    virtual bool setAnimationMode(E_BONE_ANIMATION_MODE mode) override;

    //! Gets the current animation mode of the bone
    virtual E_BONE_ANIMATION_MODE getAnimationMode() const override;

    //! returns the axis aligned bounding box of this node
    virtual const core::aabbox3d<float>& getBoundingBox() const override;

    /*
    //! Returns the relative transformation of the scene node.
    //virtual glm::mat4 getRelativeTransformation() const override;
    */

    virtual void OnAnimate(std::uint32_t timeMs) override;

    virtual void updateAbsolutePositionOfAllChildren() override;

    //! How the relative transformation of the bone is used
    virtual void setSkinningSpace(E_BONE_SKINNING_SPACE space) override
    {
      SkinningSpace = space;
    }

    virtual E_BONE_SKINNING_SPACE getSkinningSpace() const override
    {
      return SkinningSpace;
    }

  private:
    void helper_updateAbsolutePositionOfAllChildren(const std::shared_ptr<ISceneNode>& Node);

    std::uint32_t BoneIndex;
    core::aabbox3d<float> Box;

    E_BONE_ANIMATION_MODE AnimationMode;
    E_BONE_SKINNING_SPACE SkinningSpace;
  };


} // namespace scene
} // namespace saga

#endif

