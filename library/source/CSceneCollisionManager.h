// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_SCENE_COLLISION_MANAGER_H_INCLUDED__
#define __C_SCENE_COLLISION_MANAGER_H_INCLUDED__

#include "ISceneCollisionManager.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"

namespace saga
{
namespace scene
{

  //! The Scene Collision Manager provides methods for performing collision tests and picking on scene nodes.
  class CSceneCollisionManager : public ISceneCollisionManager
  {
  public:

    //! constructor
    CSceneCollisionManager(ISceneManager* smanager, video::IVideoDriver* driver);

    //! destructor
    virtual ~CSceneCollisionManager();

    //! Returns the scene node, which is currently visible at the given
    //! screen coordinates, viewed from the currently active camera.
    virtual ISceneNode* getSceneNodeFromScreenCoordinatesBB(const glm::ivec2& pos,
        std::int32_t idBitMask= 0, bool bNoDebugObjects=false, ISceneNode* root= 0) override;

    //! Returns the nearest scene node which collides with a 3d ray and
    //! whose id matches a bitmask.
    virtual ISceneNode* getSceneNodeFromRayBB(const core::line3d<float>& ray,
            std::int32_t idBitMask= 0, bool bNoDebugObjects=false,
            ISceneNode* root= 0) override;

    //! Returns the scene node, at which the given camera is looking at and
    //! which id matches the bitmask.
    virtual ISceneNode* getSceneNodeFromCameraBB(const ICameraSceneNode* camera,
        std::int32_t idBitMask= 0, bool bNoDebugObjects = false) override;

    //! Finds the nearest collision point of a line and lots of triangles, if there is one.
    virtual bool getCollisionPoint(SCollisionHit& hitResult, const core::line3d<float>& ray,
        ITriangleSelector* selector)  override;

    //! Collides a moving ellipsoid with a 3d world with gravity and returns
    //! the resulting new position of the ellipsoid.
    virtual glm::vec3 getCollisionResultPosition(
      ITriangleSelector* selector,
      const glm::vec3 &ellipsoidPosition,
      const glm::vec3& ellipsoidRadius,
      const glm::vec3& ellipsoidDirectionAndSpeed,
      core::triangle3df& triout,
      glm::vec3& hitPosition,
      bool& outFalling,
      ISceneNode*& outNode,
      float slidingSpeed,
      const glm::vec3& gravityDirectionAndSpeed) override;

    //! Returns a 3d ray which would go through the 2d screen coordinates.
    virtual core::line3d<float> getRayFromScreenCoordinates(
      const glm::ivec2& pos, const ICameraSceneNode* camera = 0) override;

    //! Calculates 2d screen position from a 3d position.
    virtual glm::ivec2 getScreenCoordinatesFrom3DPosition(
      const glm::vec3 & pos, const ICameraSceneNode* camera= 0, bool useViewPort=false) override;

    //! Gets the scene node and nearest collision point for a ray based on
    //! the nodes' id bitmasks, bounding boxes and triangle selectors.
    virtual ISceneNode* getSceneNodeAndCollisionPointFromRay(
                SCollisionHit& hitResult,
                const core::line3df& ray,
                std::int32_t idBitMask = 0,
                ISceneNode * collisionRootNode = 0,
                bool noDebugObjects = false)  override;

  private:

    //! recursive method for going through all scene nodes
    void getPickedNodeBB(ISceneNode* root, core::line3df& ray, std::int32_t bits,
          bool bNoDebugObjects,
          float& outbestdistance, ISceneNode*& outbestnode);

    //! recursive method for going through all scene nodes
    void getPickedNodeFromBBAndSelector(
            SCollisionHit& hitResult,
            ISceneNode * root,
            core::line3df & ray,
            std::int32_t bits,
            bool noDebugObjects,
            float & outBestDistanceSquared);


    struct SCollisionData
    {
      glm::vec3 eRadius;

      glm::vec3 R3Velocity;
      glm::vec3 R3Position;

      glm::vec3 velocity;
      glm::vec3 normalizedVelocity;
      glm::vec3 basePoint;

      bool foundCollision;
      float nearestDistance;
      glm::vec3 intersectionPoint;

      core::triangle3df intersectionTriangle;
      saga::scene::ISceneNode* node;
      std::int32_t triangleHits;

      float slidingSpeed;

      ITriangleSelector* selector;
    };

    //! Tests the current collision data against an individual triangle.
    /**
    \param colData: the collision data.
    \param triangle: the triangle to test against.
    \return true if the triangle is hit (and is the closest hit), false otherwise */
    bool testTriangleIntersection(SCollisionData* colData,
      const core::triangle3df& triangle);

    //! recursive method for doing collision response
    glm::vec3 collideEllipsoidWithWorld(ITriangleSelector* selector,
      const glm::vec3 &position,
      const glm::vec3& radius,  const glm::vec3& velocity,
      float slidingSpeed,
      const glm::vec3& gravity, core::triangle3df& triout,
      glm::vec3& hitPosition,
      bool& outFalling,
      ISceneNode*& outNode);

    glm::vec3 collideWithWorld(std::int32_t recursionDepth, SCollisionData &colData,
      const glm::vec3& pos, const glm::vec3& vel);

    inline bool getLowestRoot(float a, float b, float c, float maxR, float* root) const;

    ISceneManager* SceneManager;
    video::IVideoDriver* Driver;
    std::vector<core::triangle3df> Triangles; // triangle buffer
  };


} // namespace scene
} // namespace saga

#endif

