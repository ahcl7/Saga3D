// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "CColorConverter.h"
#include "SColor.h"
#include "os.h"


namespace saga
{
namespace video
{

//! converts a monochrome bitmap to A1R5G5B5 data
void CColorConverter::convert1BitTo16Bit(const u8* in, std::int16_t* out, std::int32_t width, std::int32_t height, std::int32_t linepad, bool flip)
{
  if (!in || !out)
    return;

  if (flip)
    out += width * height;

  for (std::int32_t y= 0; y<height; ++y)
  {
    std::int32_t shift = 7;
    if (flip)
      out -= width;

    for (std::int32_t x= 0; x<width; ++x)
    {
      out[x] = *in>>shift & 0x01 ? (std::int16_t)0xffff : (std::int16_t)0x8000;

      if ((--shift)<0) // 8 pixel done
      {
        shift=7;
        ++in;
      }
    }

    if (shift != 7) // width did not fill last byte
      ++in;

    if (!flip)
      out += width;
    in += linepad;
  }
}



//! converts a 4 bit palettized image to A1R5G5B5
void CColorConverter::convert4BitTo16Bit(const u8* in, std::int16_t* out, std::int32_t width, std::int32_t height, const std::int32_t* palette, std::int32_t linepad, bool flip)
{
  if (!in || !out || !palette)
    return;

  if (flip)
    out += width*height;

  for (std::int32_t y= 0; y<height; ++y)
  {
    std::int32_t shift = 4;
    if (flip)
      out -= width;

    for (std::int32_t x= 0; x<width; ++x)
    {
      out[x] = X8R8G8B8toA1R5G5B5(palette[(u8)((*in >> shift) & 0xf)]);

      if (shift== 0)
      {
        shift = 4;
        ++in;
      }
      else
        shift = 0;
    }

    if (shift == 0) // odd width
      ++in;

    if (!flip)
      out += width;
    in += linepad;
  }
}



//! converts a 8 bit palettized image into A1R5G5B5
void CColorConverter::convert8BitTo16Bit(const u8* in, std::int16_t* out, std::int32_t width, std::int32_t height, const std::int32_t* palette, std::int32_t linepad, bool flip)
{
  if (!in || !out || !palette)
    return;

  if (flip)
    out += width * height;

  for (std::int32_t y= 0; y<height; ++y)
  {
    if (flip)
      out -= width; // one line back
    for (std::int32_t x= 0; x<width; ++x)
    {
      out[x] = X8R8G8B8toA1R5G5B5(palette[(u8)(*in)]);
      ++in;
    }
    if (!flip)
      out += width;
    in += linepad;
  }
}

//! converts a 8 bit palettized or non palettized image (A8) into R8G8B8
void CColorConverter::convert8BitTo24Bit(const u8* in, u8* out, std::int32_t width, std::int32_t height, const u8* palette, std::int32_t linepad, bool flip)
{
  if (!in || !out)
    return;

  const std::int32_t lineWidth = 3 * width;
  if (flip)
    out += lineWidth * height;

  for (std::int32_t y= 0; y<height; ++y)
  {
    if (flip)
      out -= lineWidth; // one line back
    for (std::int32_t x= 0; x< lineWidth; x += 3)
    {
      if (palette)
      {
#ifdef __BIG_ENDIAN__
        out[x+0] = palette[ (in[0] << 2) + 0];
        out[x+1] = palette[ (in[0] << 2) + 1];
        out[x+2] = palette[ (in[0] << 2) + 2];
#else
        out[x+0] = palette[ (in[0] << 2) + 2];
        out[x+1] = palette[ (in[0] << 2) + 1];
        out[x+2] = palette[ (in[0] << 2) + 0];
#endif
      }
      else
      {
        out[x+0] = in[0];
        out[x+1] = in[0];
        out[x+2] = in[0];
      }
      ++in;
    }
    if (!flip)
      out += lineWidth;
    in += linepad;
  }
}

//! converts a 8 bit palettized or non palettized image (A8) into R8G8B8
void CColorConverter::convert8BitTo32Bit(const u8* in, u8* out, std::int32_t width, std::int32_t height, const u8* palette, std::int32_t linepad, bool flip)
{
  if (!in || !out)
    return;

  const std::uint32_t lineWidth = 4 * width;
  if (flip)
    out += lineWidth * height;

  std::uint32_t x;
  std::uint32_t c;
  for (std::uint32_t y= 0; y < (std::uint32_t) height; ++y)
  {
    if (flip)
      out -= lineWidth; // one line back

    if (palette)
    {
      for (x= 0; x < (std::uint32_t) width; x += 1)
      {
        c = in[x];
        ((std::uint32_t*)out)[x] = ((std::uint32_t*)palette)[ c ];
      }
    }
    else
    {
      for (x= 0; x < (std::uint32_t) width; x += 1)
      {
        c = in[x];
#ifdef __BIG_ENDIAN__
        ((std::uint32_t*)out)[x] = c << 24 | c << 16 | c << 8 | 0x000000FF;
#else
        ((std::uint32_t*)out)[x] = 0xFF000000 | c << 16 | c << 8 | c;
#endif
      }

    }

    if (!flip)
      out += lineWidth;
    in += width + linepad;
  }
}



//! converts 16bit data to 16bit data
void CColorConverter::convert16BitTo16Bit(const std::int16_t* in, std::int16_t* out, std::int32_t width, std::int32_t height, std::int32_t linepad, bool flip)
{
  if (!in || !out)
    return;

  if (flip)
    out += width * height;

  for (std::int32_t y= 0; y<height; ++y)
  {
    if (flip)
      out -= width;
#ifdef __BIG_ENDIAN__
    for (std::int32_t x= 0; x<width; ++x)
      out[x]=os::Byteswap::byteswap(in[x]);
#else
    memcpy(out, in, width*sizeof(std::int16_t));
#endif
    if (!flip)
      out += width;
    in += width;
    in += linepad;
  }
}



//! copies R8G8B8 24bit data to 24bit data
void CColorConverter::convert24BitTo24Bit(const u8* in, u8* out, std::int32_t width, std::int32_t height, std::int32_t linepad, bool flip, bool bgr)
{
  if (!in || !out)
    return;

  const std::int32_t lineWidth = 3 * width;
  if (flip)
    out += lineWidth * height;

  for (std::int32_t y= 0; y<height; ++y)
  {
    if (flip)
      out -= lineWidth;
    if (bgr)
    {
      for (std::int32_t x= 0; x<lineWidth; x+=3)
      {
        out[x+0] = in[x+2];
        out[x+1] = in[x+1];
        out[x+2] = in[x+0];
      }
    }
    else
    {
      memcpy(out,in,lineWidth);
    }
    if (!flip)
      out += lineWidth;
    in += lineWidth;
    in += linepad;
  }
}



//! Resizes the surface to a new size and converts it at the same time
//! to an A8R8G8B8 format, returning the pointer to the new buffer.
void CColorConverter::convert16bitToA8R8G8B8andResize(const std::int16_t* in, std::int32_t* out, std::int32_t newWidth, std::int32_t newHeight, std::int32_t currentWidth, std::int32_t currentHeight)
{
  if (!newWidth || !newHeight)
    return;

  // note: this is very very slow. (i didn't want to write a fast version.
  // but hopefully, nobody wants to convert surfaces every frame.

  float sourceXStep = (float)currentWidth / (float)newWidth;
  float sourceYStep = (float)currentHeight / (float)newHeight;
  float sy;
  std::int32_t t;

  for (std::int32_t x= 0; x<newWidth; ++x)
  {
    sy = 0.0f;

    for (std::int32_t y= 0; y<newHeight; ++y)
    {
      t = in[(std::int32_t)(((std::int32_t)sy)*currentWidth + x*sourceXStep)];
      t = (((t >> 15)&0x1)<<31) |  (((t >> 10)&0x1F)<<19) |
        (((t >> 5)&0x1F)<<11) |  (t&0x1F)<<3;
      out[(std::int32_t)(y*newWidth + x)] = t;

      sy+=sourceYStep;
    }
  }
}



//! copies X8R8G8B8 32 bit data
void CColorConverter::convert32BitTo32Bit(const std::int32_t* in, std::int32_t* out, std::int32_t width, std::int32_t height, std::int32_t linepad, bool flip)
{
  if (!in || !out)
    return;

  if (flip)
    out += width * height;

  for (std::int32_t y= 0; y<height; ++y)
  {
    if (flip)
      out -= width;
#ifdef __BIG_ENDIAN__
    for (std::int32_t x= 0; x<width; ++x)
      out[x]=os::Byteswap::byteswap(in[x]);
#else
    memcpy(out, in, width*sizeof(std::int32_t));
#endif
    if (!flip)
      out += width;
    in += width;
    in += linepad;
  }
}



void CColorConverter::convE_RENDER_TARGET_TYPE::A1R5G5B5toR8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  std::uint16_t* sB = (std::uint16_t*)sP;
  u8 * dB = (u8 *)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    dB[2] = (*sB & 0x7c00) >> 7;
    dB[1] = (*sB & 0x03e0) >> 2;
    dB[0] = (*sB & 0x1f) << 3;

    sB += 1;
    dB += 3;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A1R5G5B5toB8G8R8(const void* sP, std::int32_t sN, void* dP)
{
  std::uint16_t* sB = (std::uint16_t*)sP;
  u8 * dB = (u8 *)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    dB[0] = (*sB & 0x7c00) >> 7;
    dB[1] = (*sB & 0x03e0) >> 2;
    dB[2] = (*sB & 0x1f) << 3;

    sB += 1;
    dB += 3;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A1R5G5B5toA8R8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  std::uint16_t* sB = (std::uint16_t*)sP;
  std::uint32_t* dB = (std::uint32_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
    *dB++ = A1R5G5B5toA8R8G8B8(*sB++);
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A1R5G5B5toA1R5G5B5(const void* sP, std::int32_t sN, void* dP)
{
  memcpy(dP, sP, sN * 2);
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A1R5G5B5toR5G6B5(const void* sP, std::int32_t sN, void* dP)
{
  std::uint16_t* sB = (std::uint16_t*)sP;
  std::uint16_t* dB = (std::uint16_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
    *dB++ = A1R5G5B5toR5G6B5(*sB++);
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A8R8G8B8toR8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  u8* sB = (u8*)sP;
  u8* dB = (u8*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    // sB[3] is alpha
    dB[0] = sB[2];
    dB[1] = sB[1];
    dB[2] = sB[0];

    sB += 4;
    dB += 3;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A8R8G8B8toB8G8R8(const void* sP, std::int32_t sN, void* dP)
{
  u8* sB = (u8*)sP;
  u8* dB = (u8*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    // sB[3] is alpha
    dB[0] = sB[0];
    dB[1] = sB[1];
    dB[2] = sB[2];

    sB += 4;
    dB += 3;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A8R8G8B8toA8R8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  memcpy(dP, sP, sN * 4);
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A8R8G8B8toA1R5G5B5(const void* sP, std::int32_t sN, void* dP)
{
  std::uint32_t* sB = (std::uint32_t*)sP;
  std::uint16_t* dB = (std::uint16_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
    *dB++ = A8R8G8B8toA1R5G5B5(*sB++);
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A8R8G8B8toA1B5G5R5(const void* sP, std::int32_t sN, void* dP)
{
  u8 * sB = (u8 *)sP;
  std::uint16_t* dB = (std::uint16_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    std::int32_t r = sB[0] >> 3;
    std::int32_t g = sB[1] >> 3;
    std::int32_t b = sB[2] >> 3;
    std::int32_t a = sB[3] >> 3;

    dB[0] = (a << 15) | (r << 10) | (g << 5) | (b);

    sB += 4;
    dB += 1;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A8R8G8B8toR5G6B5(const void* sP, std::int32_t sN, void* dP)
{
  u8 * sB = (u8 *)sP;
  std::uint16_t* dB = (std::uint16_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    std::int32_t r = sB[2] >> 3;
    std::int32_t g = sB[1] >> 2;
    std::int32_t b = sB[0] >> 3;

    dB[0] = (r << 11) | (g << 5) | (b);

    sB += 4;
    dB += 1;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::A8R8G8B8toR3G3B2(const void* sP, std::int32_t sN, void* dP)
{
  u8* sB = (u8*)sP;
  u8* dB = (u8*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    u8 r = sB[2] & 0xe0;
    u8 g = (sB[1] & 0xe0) >> 3;
    u8 b = (sB[0] & 0xc0) >> 6;

    dB[0] = (r | g | b);

    sB += 4;
    dB += 1;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::R8G8B8toR8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  memcpy(dP, sP, sN * 3);
}

void CColorConverter::convE_RENDER_TARGET_TYPE::R8G8B8toA8R8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  u8*  sB = (u8*)sP;
  std::uint32_t* dB = (std::uint32_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    *dB = 0xff000000 | (sB[0]<<16) | (sB[1]<<8) | sB[2];

    sB += 3;
    ++dB;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::R8G8B8toA1R5G5B5(const void* sP, std::int32_t sN, void* dP)
{
  u8 * sB = (u8 *)sP;
  std::uint16_t* dB = (std::uint16_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    std::int32_t r = sB[0] >> 3;
    std::int32_t g = sB[1] >> 3;
    std::int32_t b = sB[2] >> 3;

    dB[0] = (0x8000) | (r << 10) | (g << 5) | (b);

    sB += 3;
    dB += 1;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::B8G8R8toA8R8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  u8*  sB = (u8*)sP;
  std::uint32_t* dB = (std::uint32_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    *dB = 0xff000000 | (sB[2]<<16) | (sB[1]<<8) | sB[0];

    sB += 3;
    ++dB;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::B8G8R8A8toA8R8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  u8* sB = (u8*)sP;
  u8* dB = (u8*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    dB[0] = sB[3];
    dB[1] = sB[2];
    dB[2] = sB[1];
    dB[3] = sB[0];

    sB += 4;
    dB += 4;
  }

}

void CColorConverter::convE_RENDER_TARGET_TYPE::A8R8G8B8toA8B8G8R8(const void* sP, std::int32_t sN, void* dP)
{
  const std::uint32_t* sB = (const std::uint32_t*)sP;
  std::uint32_t* dB = (std::uint32_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    *dB++ = (*sB & 0xff00ff00) | ((*sB & 0x00ff0000) >> 16) | ((*sB & 0x000000ff) << 16);
    ++sB;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::R8G8B8toR5G6B5(const void* sP, std::int32_t sN, void* dP)
{
  u8 * sB = (u8 *)sP;
  std::uint16_t* dB = (std::uint16_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    std::int32_t r = sB[0] >> 3;
    std::int32_t g = sB[1] >> 2;
    std::int32_t b = sB[2] >> 3;

    dB[0] = (r << 11) | (g << 5) | (b);

    sB += 3;
    dB += 1;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::R5G6B5toR5G6B5(const void* sP, std::int32_t sN, void* dP)
{
  memcpy(dP, sP, sN * 2);
}

void CColorConverter::convE_RENDER_TARGET_TYPE::R5G6B5toR8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  std::uint16_t* sB = (std::uint16_t*)sP;
  u8 * dB = (u8 *)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    dB[0] = (*sB & 0xf800) >> 8;
    dB[1] = (*sB & 0x07e0) >> 3;
    dB[2] = (*sB & 0x001f) << 3;

    sB += 1;
    dB += 3;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::R5G6B5toB8G8R8(const void* sP, std::int32_t sN, void* dP)
{
  std::uint16_t* sB = (std::uint16_t*)sP;
  u8 * dB = (u8 *)dP;

  for (std::int32_t x = 0; x < sN; ++x)
  {
    dB[2] = (*sB & 0xf800) >> 8;
    dB[1] = (*sB & 0x07e0) >> 3;
    dB[0] = (*sB & 0x001f) << 3;

    sB += 1;
    dB += 3;
  }
}

void CColorConverter::convE_RENDER_TARGET_TYPE::R5G6B5toA8R8G8B8(const void* sP, std::int32_t sN, void* dP)
{
  std::uint16_t* sB = (std::uint16_t*)sP;
  std::uint32_t* dB = (std::uint32_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
    *dB++ = R5G6B5toA8R8G8B8(*sB++);
}

void CColorConverter::convE_RENDER_TARGET_TYPE::R5G6B5toA1R5G5B5(const void* sP, std::int32_t sN, void* dP)
{
  std::uint16_t* sB = (std::uint16_t*)sP;
  std::uint16_t* dB = (std::uint16_t*)dP;

  for (std::int32_t x = 0; x < sN; ++x)
    *dB++ = R5G6B5toA1R5G5B5(*sB++);
}


void CColorConverter::convE_RENDER_TARGET_TYPE::viaFormat(const void* sP, E_PIXEL_FORMAT sF, std::int32_t sN,
        void* dP, E_PIXEL_FORMAT dF)
{
  switch (sF)
  {
    case E_PIXEL_FORMAT::A1R5G5B5:
      switch (dF)
      {
        case E_PIXEL_FORMAT::A1R5G5B5:
          convE_RENDER_TARGET_TYPE::A1R5G5B5toA1R5G5B5(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R5G6B5:
          convE_RENDER_TARGET_TYPE::A1R5G5B5toR5G6B5(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R8G8B8A8:
          convE_RENDER_TARGET_TYPE::A1R5G5B5toA8R8G8B8(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R8G8B8:
          convE_RENDER_TARGET_TYPE::A1R5G5B5toR8G8B8(sP, sN, dP);
        break;
#ifndef _DEBUG
        default:
          break;
#endif
      }
    break;
    case E_PIXEL_FORMAT::R5G6B5:
      switch (dF)
      {
        case E_PIXEL_FORMAT::A1R5G5B5:
          convE_RENDER_TARGET_TYPE::R5G6B5toA1R5G5B5(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R5G6B5:
          convE_RENDER_TARGET_TYPE::R5G6B5toR5G6B5(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R8G8B8A8:
          convE_RENDER_TARGET_TYPE::R5G6B5toA8R8G8B8(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R8G8B8:
          convE_RENDER_TARGET_TYPE::R5G6B5toR8G8B8(sP, sN, dP);
        break;
#ifndef _DEBUG
        default:
          break;
#endif
      }
    break;
    case E_PIXEL_FORMAT::R8G8B8A8:
      switch (dF)
      {
        case E_PIXEL_FORMAT::A1R5G5B5:
          convE_RENDER_TARGET_TYPE::A8R8G8B8toA1R5G5B5(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R5G6B5:
          convE_RENDER_TARGET_TYPE::A8R8G8B8toR5G6B5(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R8G8B8A8:
          convE_RENDER_TARGET_TYPE::A8R8G8B8toA8R8G8B8(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R8G8B8:
          convE_RENDER_TARGET_TYPE::A8R8G8B8toR8G8B8(sP, sN, dP);
        break;
#ifndef _DEBUG
        default:
          break;
#endif
      }
    break;
    case E_PIXEL_FORMAT::R8G8B8:
      switch (dF)
      {
        case E_PIXEL_FORMAT::A1R5G5B5:
          convE_RENDER_TARGET_TYPE::R8G8B8toA1R5G5B5(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R5G6B5:
          convE_RENDER_TARGET_TYPE::R8G8B8toR5G6B5(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R8G8B8A8:
          convE_RENDER_TARGET_TYPE::R8G8B8toA8R8G8B8(sP, sN, dP);
        break;
        case E_PIXEL_FORMAT::R8G8B8:
          convE_RENDER_TARGET_TYPE::R8G8B8toR8G8B8(sP, sN, dP);
        break;
#ifndef _DEBUG
        default:
          break;
#endif
      }
    break;
  }
}


} // namespace video
} // namespace saga
