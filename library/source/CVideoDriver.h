// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __IRR_CVIDEO_DRIVER_H_INCLUDED__
#define __IRR_CVIDEO_DRIVER_H_INCLUDED__

#include "IVideoDriver.h"
#include "SIrrCreationParameters.h"
#include "ISceneManager.h"
#include <unordered_map>

namespace saga
{
namespace video
{
  class CVideoDriver : public IVideoDriver
  {
  public:
    CVideoDriver(const SIrrlichtCreationParameters& params);
    virtual ~CVideoDriver();

    virtual void beginPass(RenderPassHandle pass) override;
    virtual void endPass(bool render = true) override;

    virtual SRenderPass createRenderPass() const override;
    virtual SGPUResource::HandleType createResource(SRenderPass&& pass) override;

    virtual SPipeline createPipeline() const override;
    virtual SGPUResource::HandleType createResource(SPipeline&& pipeline) override;

    virtual STexture createTexture() override;
    virtual TextureHandle createTexture(STexture&& texture) override;
    virtual TextureHandle createTexture(const std::string& path) override;
    virtual TextureHandle createTexture(unsigned char* data, std::size_t size) override;
  
    virtual SShader createShader() const override;
    virtual SGPUResource::HandleType createResource(SShader&& shader) override;
    virtual SShaderUniform createShaderUniform() const override;
    virtual SGPUResource::HandleType createResource(SShaderUniform&& shader) override;

    virtual void addShader(SShader& shader, E_SHADER_TYPE type, std::string&& source) const override;
    virtual void addShaderFromFile(SShader& shader, E_SHADER_TYPE type, const std::string& path) const override;

    virtual void setSceneManager(const std::shared_ptr<scene::ISceneManager>& smgr) override
    { SceneManager = smgr; }

    auto& getPipeline(PipelineHandle p) { return Pipelines[p]; }

  protected:
    SIrrlichtCreationParameters CreationParams;
    std::unordered_map<SGPUResource::HandleType, SRenderPass> RenderPasses;
    std::unordered_map<SGPUResource::HandleType, SPipeline> Pipelines;
    std::unordered_map<SGPUResource::HandleType, SShader> Shaders;
    std::unordered_map<SGPUResource::HandleType, SShaderUniform> ShaderUniforms;
    std::unordered_map<SGPUResource::HandleType, STexture> Textures;
    std::shared_ptr<scene::ISceneManager> SceneManager = nullptr;

    RenderPassHandle CurrentPass;

  private:
    SGPUResource::HandleType ID = 1;
  };

} // namespace video
} // namespace saga


#endif
