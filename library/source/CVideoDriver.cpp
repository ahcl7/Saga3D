#include "CVideoDriver.h"
#include "SRenderPassState.h"
#include "SRenderPass.h"
#include "EShaderTypes.h"
#include "SShader.h"
#include "SShaderUniform.h"
#include "ICameraSceneNode.h"
#include <stb_image.h>
#include <fstream>
#include <streambuf>

namespace saga
{
namespace video
{

CVideoDriver::CVideoDriver(const SIrrlichtCreationParameters& params)
  :  CreationParams(params)
{

}

CVideoDriver::~CVideoDriver()
{

}

SRenderPass CVideoDriver::createRenderPass() const
{
  return {};
}

SPipeline CVideoDriver::createPipeline() const
{
  return {};
}

STexture CVideoDriver::createTexture()
{
  return {};
}

TextureHandle CVideoDriver::createTexture(unsigned char* data, std::size_t size)
{
  STexture texture;
  int channels;
  auto pixelData = stbi_load_from_memory(data, size, &texture.Width, &texture.Height, &channels, 4);
  auto& textureData = texture.Contents[0][0].Data;
  auto memSize = texture.Width * texture.Height * sizeof(char) * 4;
  textureData.reserve(memSize);
  memcpy(textureData.data(), pixelData, memSize);
  stbi_image_free(pixelData);
  texture.Format = E_PIXEL_FORMAT::R8G8B8A8;
  Textures[ID] = std::move(texture);
  return ID++;
}

TextureHandle CVideoDriver::createTexture(const std::string& path)
{
  std::ifstream file(path, std::ios::binary | std::ios::ate);
  auto size = file.tellg();
  file.seekg(0, std::ios::beg);

  std::vector<unsigned char> buffer(size);
  if (file.read((char*) buffer.data(), size))
  {
    return createTexture(buffer.data(), size);
  }
  else
  {
    return NULL_GPU_RESOURCE_HANDLE;
  }
}

SShader CVideoDriver::createShader() const
{
  return {};
}

SShaderUniform CVideoDriver::createShaderUniform() const
{
  return {};
}

TextureHandle CVideoDriver::createTexture(STexture&& texture)
{
  Textures[ID] = std::move(texture);
  return ID++;
}

void CVideoDriver::addShader(SShader& shader, E_SHADER_TYPE type, std::string&& source) const
{
  switch (type)
  {
    case E_SHADER_TYPE::VERTEX: shader.VSSource = std::move(source); break;
    case E_SHADER_TYPE::GEOMETRY: shader.GSSource = std::move(source); break;
    case E_SHADER_TYPE::FRAGMENT: shader.FSSource = std::move(source); break;
    case E_SHADER_TYPE::COMPUTE: shader.CSource = std::move(source); break;
  }
}

void CVideoDriver::addShaderFromFile(SShader& shader, E_SHADER_TYPE type, const std::string& path) const
{
  std::ifstream file(path);
  std::string source(
    (std::istreambuf_iterator<char>(file)),
    std::istreambuf_iterator<char>()
  );
  switch (type)
  {
    case E_SHADER_TYPE::VERTEX: shader.VSSource = std::move(source); break;
    case E_SHADER_TYPE::GEOMETRY: shader.GSSource = std::move(source); break;
    case E_SHADER_TYPE::FRAGMENT: shader.FSSource = std::move(source); break;
    case E_SHADER_TYPE::COMPUTE: shader.CSource = std::move(source); break;
  }
}

SGPUResource::HandleType CVideoDriver::createResource(SRenderPass&& pass)
{
  pass.Handle = ID;
  RenderPasses[ID] = std::move(pass);
  return ID++;
}

SGPUResource::HandleType CVideoDriver::createResource(SPipeline&& pipeline)
{
  pipeline.Handle = ID;
  Pipelines[ID] = std::move(pipeline);
  return ID++;
}

SGPUResource::HandleType CVideoDriver::createResource(SShader&& shader)
{
  shader.Handle = ID;
  Shaders[ID] = std::move(shader);
  return ID++;
}

SGPUResource::HandleType CVideoDriver::createResource(SShaderUniform&& uniform)
{
  uniform.Handle = ID;
  ShaderUniforms[ID] = std::move(uniform);
  return ID++;
}

void CVideoDriver::beginPass(RenderPassHandle pass)
{
  CurrentPass = pass;
}

void CVideoDriver::endPass(bool render)
{
  CurrentPass = NULL_GPU_RESOURCE_HANDLE;
}

} // namespace video
} // namespace saga

