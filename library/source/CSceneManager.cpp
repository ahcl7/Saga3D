#include "CSceneManager.h"
#include "CMeshCache.h"
#include "CAssimpMeshLoader.h"
#include "CMeshSceneNode.h"
#include "CCameraSceneNode.h"
#include "CSceneNodeAnimatorCameraFPS.h"
#include <SDL2/SDL.h>
#include <fstream>

namespace saga
{
namespace scene
{

CSceneManager::CSceneManager(const std::shared_ptr<video::IVideoDriver>& driver)
  :  Driver(driver), MeshCache(std::make_unique<CMeshCache>())
{
  MeshLoaderList.push_back(std::make_unique<CAssimpMeshLoader>());
}

CSceneManager::~CSceneManager()
{

}

void CSceneManager::OnEvent(const SDL_Event& event)
{
  for (auto& cam : CameraList)
  {
    cam->OnEvent(event);
  }
}

void CSceneManager::registerSceneNode(const std::shared_ptr<ISceneNode>& node, video::RenderPassHandle pass)
{
  node->OnRegisterSceneNode(pass);
  NodeList[pass][node->getPipeline()].push_back(node);
}

void CSceneManager::unregisterSceneNode(const std::shared_ptr<ISceneNode>& node, video::RenderPassHandle pass)
{
  auto& nodeList = NodeList[pass][node->getPipeline()];
  nodeList.erase(
    std::remove_if(nodeList.begin(), nodeList.end(), [&node] (auto& n) {
      return n.get() == node.get();
    }), nodeList.end()
  );
}

const std::shared_ptr<IAnimatedMesh>& CSceneManager::getMesh(const std::string& fileName, const std::string& meshName)
{
  std::ifstream file(fileName, std::ios::binary | std::ios::ate);
  auto size = file.tellg();
  file.seekg(0, std::ios::beg);

  std::vector<char> buffer(size);
  if (file.read(buffer.data(), size))
  {
    auto extension = fileName.substr(fileName.find_last_of("."));
    if (meshName.empty())
    {
      return getMesh(buffer.data(), buffer.size(), extension, fileName);
    }
    else
    {
      return getMesh(buffer.data(), buffer.size(), extension, meshName);
    }
  }
  else
    exit(1);
}

const std::shared_ptr<IAnimatedMesh>& CSceneManager::getMesh(void* data, const std::size_t size,
  const std::string& extension, const std::string& name)
{
  auto mesh = MeshCache->getMeshByName(name);
  if (mesh) return MeshCache->getMeshByName(name);

  // iterate the list in reverse order so user-added loaders can override the built-in ones
  for (auto& loader : MeshLoaderList)
  {
    if (loader->isSupportedExtension(extension))
    {
      mesh = loader->createMesh(data, size);
      if (mesh)
      {
        MeshCache->addMesh(name, mesh);
        return MeshCache->getMeshByName(name);
      }
      else
        exit(-1);
    }
  }
}

std::shared_ptr<IMeshSceneNode> CSceneManager::createMeshSceneNode(const std::shared_ptr<IMesh>& mesh, const std::shared_ptr<ISceneNode>& parent,
  const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale)
{
  return std::make_shared<CMeshSceneNode>(mesh, parent, shared_from_this(), position, rotation, scale);
}

const std::shared_ptr<ICameraSceneNode>& CSceneManager::addCameraSceneNode(
  const std::shared_ptr<ISceneNode> &parent, const glm::vec3 &position,
  const glm::vec3 &lookAt, bool makeActive)
{
  auto cam = std::make_shared<CCameraSceneNode>(parent, shared_from_this(), position, lookAt);
  CameraList.push_back(cam);
  if (makeActive)
    setActiveCamera(cam);
  return CameraList.back();
}

const std::shared_ptr<ICameraSceneNode>& CSceneManager::addCameraSceneNodeFPS(
  const std::shared_ptr<ISceneNode>& parent,
  float moveSpeed, float rotateSpeed,
  bool makeActive)
{
  auto cam = std::make_shared<CCameraSceneNode>(parent, shared_from_this());
  auto anim = std::make_shared<CSceneNodeAnimatorCameraFPS>(
    moveSpeed, rotateSpeed
  );
  cam->addAnimator(anim);
  CameraList.push_back(cam);
  if (makeActive)
    setActiveCamera(cam);
  return CameraList.back();
}

void CSceneManager::setActiveCamera(const std::shared_ptr<ICameraSceneNode>& cam)
{
  ActiveCamera = cam;
}

void CSceneManager::animate()
{
  const auto time = SDL_GetTicks();
  ActiveCamera->OnAnimate(time);
  for (auto& it : NodeList)
  {
    for (auto& pipeline : it.second)
    {
      for (auto& node : pipeline.second)
      {
        node->OnAnimate(time);
        node->updateAbsolutePosition();
      }
    }
  }
}

} // namespace scene
} // namespace saga
