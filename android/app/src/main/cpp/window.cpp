#include <Saga.h>
#include <chrono>

using namespace saga;
using namespace core;
using namespace video;

int main(int argc, char* argv[]) {
  auto device = createDevice(E_DRIVER_TYPE::VULKAN, {800, 600}, 16, false, false, false);
  device->setWindowCaption("A Saga Window");

  auto& driver = device->getVideoDriver();
  auto passInfo = driver->createRenderPass();

  passInfo.State.Colors[0] = {
    E_RENDER_PASS_STATE::CLEAR,
    { 0.5f, 0.5f, 0.5f, 1.f }
  };
  
  auto pass = driver->createResource(std::move(passInfo));

  auto Then = std::chrono::high_resolution_clock::now();
  while (device->run()) {
    driver->beginPass(pass);
    auto Now = std::chrono::high_resolution_clock::now();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(Now - Then).count();
    Then = Now;
    if (millis > 0)
    {
      auto sec = millis / 1000.f;
      int fps = 60.f / sec;
      device->setWindowCaption("Saga 3D Window - FPS: " + std::to_string(fps));
    }
    driver->endPass();
  }

  return 0;
}

