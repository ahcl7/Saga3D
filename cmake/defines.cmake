include_guard()

#  _IRR_MATERIAL_MAX_TEXTURES_=8
#  IRRLICHT_SDK_VERSION=\"1.9.0\"
#  IRRCALLCONV=1

add_definitions(-DGLM_FORCE_DEPTH_ZERO_TO_ONE)
add_definitions(-DGLM_ENABLE_EXPERIMENTAL)
add_definitions(-DSTB_IMAGE_IMPLEMENTATION)